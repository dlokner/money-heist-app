package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.MemberSkill;
import com.ag04.moneyheist.dto.MemberSkillDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * {@code MemberSkillDto} to {@code HeistSkill} converter.
 *
 * @author dlokner
 */
@Component
public class MemberSkillToMemberSkillDto implements Converter<MemberSkill, MemberSkillDto> {

    @Override
    public MemberSkillDto convert(MemberSkill source) {
        if (Objects.isNull(source)) return null;

        MemberSkillDto memberSkillDto = new MemberSkillDto();
        memberSkillDto.setId(source.getId());
        memberSkillDto.setName(source.getName());
        memberSkillDto.setLevel(source.getLevel());
        return memberSkillDto;
    }
}
