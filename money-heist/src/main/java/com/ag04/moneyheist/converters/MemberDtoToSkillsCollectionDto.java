package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.dto.SkillsCollectionDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@code MemberDto} to {@code SkillCollectionDto} converter class
 *
 * @author dlokner
 */
@Component
public class MemberDtoToSkillsCollectionDto implements Converter<MemberDto, SkillsCollectionDto<MemberSkillDto>> {

    @Override
    public SkillsCollectionDto<MemberSkillDto> convert(MemberDto source) {
        SkillsCollectionDto<MemberSkillDto> collection = new SkillsCollectionDto<>();
        collection.setSkills(source.getSkills());
        return collection;
    }
}
