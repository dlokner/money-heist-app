package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.HeistSkill;
import com.ag04.moneyheist.dto.HeistSkillDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@code HeistSkillDto} to {@code HeistSkill} converter.
 *
 * @author dlokner
 */
@Component
public class HeistSkillDtoToHeistSkill implements Converter<HeistSkillDto, HeistSkill> {

    @Override
    public HeistSkill convert(HeistSkillDto source) {
        HeistSkill heistSkill = new HeistSkill();
        heistSkill.setMembers(source.getMembers());
        heistSkill.setId(source.getId());
        heistSkill.setLevel(source.getLevel());
        heistSkill.setName(source.getName());
        return heistSkill;
    }
}
