package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.Member;
import com.ag04.moneyheist.domain.MemberSkill;
import com.ag04.moneyheist.domain.Status;
import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@code MemberDto} to {@code Member} converter.
 *
 * @author dlokner
 */
@Component
public class MemberDtoToMember implements Converter<MemberDto, Member> {

    private final MemberSkillDtoToMemberSkill memberSkillDtoToMemberSkill;

    public MemberDtoToMember(MemberSkillDtoToMemberSkill memberSkillDtoToMemberSkill) {
        this.memberSkillDtoToMemberSkill = memberSkillDtoToMemberSkill;
    }

    @Override
    public Member convert(MemberDto source) {
        if (Objects.isNull(source)) return null;

        Member member = new Member();

        member.setId(source.getId());
        member.setName(source.getName());
        member.setEmail(source.getEmail());
        member.setSex(source.getSex());
        member.setStatus(Status.valueOf(source.getStatus()));

        member.setMainSkill(source.getMainSkill());
        member.setSkills(skillsSetConverter(source.getSkills()));
        member.getSkills().forEach(skill -> skill.setOwner(member));

        return member;
    }

    public Set<MemberSkill> skillsSetConverter(List<MemberSkillDto> skillsDto) {
        if (Objects.isNull(skillsDto)) return new HashSet<>();
        return skillsDto.stream()
                .map(memberSkillDtoToMemberSkill::convert)
                .collect(Collectors.toSet());
    }
}
