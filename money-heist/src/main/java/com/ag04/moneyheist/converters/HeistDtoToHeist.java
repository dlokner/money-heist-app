package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.Heist;
import com.ag04.moneyheist.domain.HeistSkill;
import com.ag04.moneyheist.domain.HeistStatus;
import com.ag04.moneyheist.domain.Member;
import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.HeistSkillDto;
import com.ag04.moneyheist.dto.MemberDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@code HeistDto} to {@code Heist} converter.
 *
 * @author dlokner
 */
@Component
public class HeistDtoToHeist implements Converter<HeistDto, Heist> {

    private final HeistSkillDtoToHeistSkill heistSkillDtoToHeistSkill;
    private final MemberDtoToMember memberDtoToMember;

    public HeistDtoToHeist(HeistSkillDtoToHeistSkill heistSkillDtoToHeistSkill, MemberDtoToMember memberDtoToMember) {
        this.heistSkillDtoToHeistSkill = heistSkillDtoToHeistSkill;
        this.memberDtoToMember = memberDtoToMember;
    }

    @Override
    public Heist convert(HeistDto source) {
        Heist heist = new Heist();
        heist.setId(source.getId());
        heist.setName(source.getName());
        heist.setLocation(source.getLocation());
        heist.setStartTime(Instant.parse(source.getStartTime()));
        heist.setEndTime(Instant.parse(source.getEndTime()));

        heist.setSkills(skillsDtoToSkills(source.getSkills()));
        heist.getSkills().forEach(s -> s.setHeist(heist));

        heist.setMembers(membersDtoToMember(source.getMembers()));

        heist.setStatus(HeistStatus.valueOf(source.getStatus()));
        return heist;
    }

    private Set<HeistSkill> skillsDtoToSkills(List<HeistSkillDto> skillsDto) {
        if (Objects.isNull(skillsDto)) return new HashSet<>();
        return skillsDto.stream().map(heistSkillDtoToHeistSkill::convert).collect(Collectors.toSet());
    }

    private Set<Member> membersDtoToMember(List<MemberDto> membersDto) {
        if (Objects.isNull(membersDto)) return new HashSet<>();
        return membersDto.stream().map(memberDtoToMember::convert).collect(Collectors.toSet());
    }
}
