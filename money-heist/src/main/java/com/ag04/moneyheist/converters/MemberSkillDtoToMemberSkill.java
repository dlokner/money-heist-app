package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.MemberSkill;
import com.ag04.moneyheist.dto.MemberSkillDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * {@code HeistSkill} to {@code MemberSkillDto} converter.
 *
 * @author dlokner
 */
@Component
public class MemberSkillDtoToMemberSkill implements Converter<MemberSkillDto, MemberSkill> {

    @Override
    public MemberSkill convert(MemberSkillDto source) {
        if (Objects.isNull(source)) return null;

        MemberSkill skill = new MemberSkill();
        skill.setId(source.getId());
        skill.setName(source.getName());
        skill.setLevel(source.getLevel());
        return skill;
    }
}
