package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.Heist;
import com.ag04.moneyheist.domain.HeistSkill;
import com.ag04.moneyheist.domain.Member;
import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.HeistSkillDto;
import com.ag04.moneyheist.dto.MemberDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@code Heist} to {@code HeistDto} converter.
 *
 * @author dlokner
 */
@Component
public class HeistToHeistDto implements Converter<Heist, HeistDto> {

    private final HeistSkillToHeistSkillDto heistSkillToHeistSkillDto;
    private final MemberToMemberDto memberToMemberDto;

    public HeistToHeistDto(HeistSkillToHeistSkillDto heistSkillToHeistSkillDto, MemberToMemberDto memberToMemberDto) {
        this.heistSkillToHeistSkillDto = heistSkillToHeistSkillDto;
        this.memberToMemberDto = memberToMemberDto;
    }

    @Override
    public HeistDto convert(Heist source) {
        HeistDto dto = new HeistDto();
        dto.setId(source.getId());
        dto.setName(source.getName());
        dto.setLocation(source.getLocation());
        dto.setStartTime(source.getStartTime().toString());
        dto.setEndTime(source.getEndTime().toString());
        dto.setSkills(skillsToSkillsDto(source.getSkills()));
        dto.setStatus(source.getStatus().name());
        dto.setMembers(membersToMembersDto(source.getMembers()));
        return dto;
    }

    private List<HeistSkillDto> skillsToSkillsDto(Set<HeistSkill> skills) {
        if (Objects.isNull(skills)) return new LinkedList<>();
        return skills.stream().map(heistSkillToHeistSkillDto::convert).collect(Collectors.toList());
    }

    private List<MemberDto> membersToMembersDto(Set<Member> members) {
        if (Objects.isNull(members)) return new LinkedList<>();
        return members.stream().map(memberToMemberDto::convert).collect(Collectors.toList());
    }
}