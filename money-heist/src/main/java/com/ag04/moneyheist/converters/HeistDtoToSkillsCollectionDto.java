package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.HeistSkillDto;
import com.ag04.moneyheist.dto.SkillsCollectionDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@code HeistDto} to {@code SkillCollectionDto} converter class
 *
 * @author dlokner
 */
@Component
public class HeistDtoToSkillsCollectionDto implements Converter<HeistDto, SkillsCollectionDto<HeistSkillDto>> {

    @Override
    public SkillsCollectionDto<HeistSkillDto> convert(HeistDto source) {
        SkillsCollectionDto<HeistSkillDto> collection = new SkillsCollectionDto<>();
        collection.setSkills(source.getSkills());
        return collection;
    }
}