package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.dto.MemberSkillsCollectionDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@code MemberDto} to {@code SkillCollectionDto} converter class
 *
 * @author dlokner
 */
@Component
public class MemberDtoToMembersSkillsCollectionDto implements Converter<MemberDto, MemberSkillsCollectionDto<MemberSkillDto>> {

    @Override
    public MemberSkillsCollectionDto<MemberSkillDto> convert(MemberDto source) {
        MemberSkillsCollectionDto<MemberSkillDto> collection = new MemberSkillsCollectionDto<>();
        collection.setSkills(source.getSkills());
        collection.setMainSkill(source.getMainSkill());
        return collection;
    }
}
