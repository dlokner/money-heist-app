package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.HeistSkill;
import com.ag04.moneyheist.dto.HeistSkillDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@code HeistSkillDto} to {@code HeistSkill} converter.
 *
 * @author dlokner
 */
@Component
public class HeistSkillToHeistSkillDto implements Converter<HeistSkill, HeistSkillDto> {

    @Override
    public HeistSkillDto convert(HeistSkill source) {
        HeistSkillDto dto = new HeistSkillDto();
        dto.setMembers(source.getMembers());
        dto.setName(source.getName());
        dto.setId(source.getId());
        dto.setLevel(source.getLevel());
        return dto;
    }
}
