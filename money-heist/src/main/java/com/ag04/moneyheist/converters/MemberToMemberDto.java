package com.ag04.moneyheist.converters;

import com.ag04.moneyheist.domain.Member;
import com.ag04.moneyheist.domain.MemberSkill;
import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@code Member} to {@code MemberDto} converter.
 *
 * @author dlokner
 */
@Component
public class MemberToMemberDto implements Converter<Member, MemberDto> {

    private final MemberSkillToMemberSkillDto memberSkillToMemberSkillDto;

    public MemberToMemberDto(MemberSkillToMemberSkillDto memberSkillToMemberSkillDto) {
        this.memberSkillToMemberSkillDto = memberSkillToMemberSkillDto;
    }

    @Override
    public MemberDto convert(Member source) {
        if (Objects.isNull(source)) return null;

        MemberDto memberDto = new MemberDto();

        memberDto.setId(source.getId());
        memberDto.setSex(source.getSex());
        memberDto.setEmail(source.getEmail());
        memberDto.setMainSkill(source.getMainSkill());
        memberDto.setStatus(source.getStatus().name());
        memberDto.setName(source.getName());

        memberDto.setSkills(skillsSetConverter(source.getSkills()));

        return memberDto;
    }

    public List<MemberSkillDto> skillsSetConverter(Set<MemberSkill> skills) {
        if (Objects.isNull(skills)) return new LinkedList<>();
        return skills.stream()
                .map(memberSkillToMemberSkillDto::convert)
                .collect(Collectors.toList());
    }
}
