package com.ag04.moneyheist.dto;

import lombok.Data;

/**
 * DTO for {@code HeistStatus} objects
 *
 * @author dlokner
 */
@Data
public class HeistStatusDto {
    public String status;
}
