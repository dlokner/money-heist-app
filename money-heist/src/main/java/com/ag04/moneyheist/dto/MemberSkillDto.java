package com.ag04.moneyheist.dto;

import lombok.EqualsAndHashCode;

/**
 * DTO for {@code MemberSkill} class objects.
 *
 * @see com.ag04.moneyheist.domain.Skill
 * @see com.ag04.moneyheist.domain.MemberSkill
 * @author dlokner
 */
@EqualsAndHashCode(callSuper = true)
public class MemberSkillDto extends SkillDto {
}
