package com.ag04.moneyheist.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * DTO for {@code Member} object used in eligible members view
 *
 * @author dlokner
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EligibleMemberDto {
    public String name;
    public Collection<MemberSkillDto> skills;
}
