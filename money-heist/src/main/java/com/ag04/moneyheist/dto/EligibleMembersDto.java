package com.ag04.moneyheist.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * DTO used for potential heist members transfer.
 *
 * @author dlokner
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EligibleMembersDto {
    Collection<HeistSkillDto> skills;
    Collection<EligibleMemberDto> members;
}
