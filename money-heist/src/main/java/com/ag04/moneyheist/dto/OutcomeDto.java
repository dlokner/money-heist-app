package com.ag04.moneyheist.dto;

import com.ag04.moneyheist.domain.Outcome;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for {@code Heist} Outcome
 *
 * @author dlokner
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OutcomeDto {
    Outcome outcome;
}
