package com.ag04.moneyheist.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Positive;

/**
 * DTO for {@code HeistSkill} class objects.
 *
 * @see com.ag04.moneyheist.domain.Skill
 * @see com.ag04.moneyheist.domain.HeistSkill
 * @author dlokner
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class HeistSkillDto extends SkillDto {

    @Positive
    private Integer members;
}
