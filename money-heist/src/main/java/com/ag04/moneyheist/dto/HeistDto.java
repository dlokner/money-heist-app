package com.ag04.moneyheist.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * DTO for {@code Heist} class.
 *
 * @see com.ag04.moneyheist.domain.Heist
 * @author dlokner
 */
@Data
public class HeistDto {

    private Long id;

    @NotBlank
    private String name;

    private String location;

    private String startTime;

    private String endTime;

    @Valid
    @NotNull
    private List<HeistSkillDto> skills;

    private String status;

    @JsonIgnore
    private List<MemberDto> members;
}
