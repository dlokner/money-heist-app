package com.ag04.moneyheist.dto;

import lombok.Data;

import java.util.Collection;

/**
 * DTO for collection of Skills
 *
 * @param <T> type of skill objects
 */
@Data
public class SkillsCollectionDto <T extends SkillDto> {
    public Collection<T> skills;
}
