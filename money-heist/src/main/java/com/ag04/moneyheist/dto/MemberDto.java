package com.ag04.moneyheist.dto;

import com.ag04.moneyheist.domain.Sex;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * DTO for {@code Member} class objects.
 *
 * @see com.ag04.moneyheist.domain.Member
 * @author dlokner
 */
@Data
public class MemberDto {

    private Long id;

    @NotBlank
    private String name;

    @NotNull
    private Sex sex;

    @Email
    private String email;

    @Valid
    @NotNull
    private List<MemberSkillDto> skills;

    private String mainSkill;

    @NotNull
    private String status;
}
