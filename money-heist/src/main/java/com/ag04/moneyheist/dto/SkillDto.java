package com.ag04.moneyheist.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * DTO for {@code Skill} class objects.
 *
 * @see com.ag04.moneyheist.domain.Skill
 * @see com.ag04.moneyheist.domain.MemberSkill
 * @see com.ag04.moneyheist.domain.HeistSkill
 * @author dlokner
 */
@Data
public class SkillDto {

    private Long id;

    @NotBlank
    private String name;

    @Size(max = 10)
    @Pattern(regexp = "\\*+")
    private String level = "*";
}
