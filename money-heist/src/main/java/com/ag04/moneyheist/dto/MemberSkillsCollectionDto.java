package com.ag04.moneyheist.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * DTO for collection of Member Skills
 *
 * @param <T> type of skill objects
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MemberSkillsCollectionDto<T extends SkillDto> extends SkillsCollectionDto<T> {
    public String mainSkill;
}
