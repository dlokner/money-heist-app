package com.ag04.moneyheist.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * JavaBean domain model representing a skill needed for the heist.
 *
 * @author dlokner
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true, exclude = "heist")
public class HeistSkill extends Skill {

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    private Heist heist;

    private Integer members;
}