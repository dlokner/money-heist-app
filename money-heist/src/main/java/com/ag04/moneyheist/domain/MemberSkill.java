package com.ag04.moneyheist.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * JavaBean domain model representing a skill of the heist member.
 *
 * @author dlokner
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true, exclude = "owner")
public class MemberSkill extends Skill {

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    private Member owner;
}
