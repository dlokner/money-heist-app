package com.ag04.moneyheist.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

/**
 * JavaBean domain model representing a heist member.
 *
 * @author dlokner
 */
@Entity
@Data
@EqualsAndHashCode(exclude = {"heists"})
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(unique = true)
    private String email;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
    private Set<MemberSkill> skills = new HashSet<>();

    private String mainSkill;

    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToMany
    @JsonIgnore
    @ToString.Exclude
    private Set<Heist> heists;
}
