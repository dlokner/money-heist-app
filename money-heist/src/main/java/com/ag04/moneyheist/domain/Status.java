package com.ag04.moneyheist.domain;

/**
 * Enumerates heist member status.
 *
 * @author dlokner
 */
public enum Status {
    AVAILABLE,
    EXPIRED,
    INCARCERATED,
    RETIRED
}
