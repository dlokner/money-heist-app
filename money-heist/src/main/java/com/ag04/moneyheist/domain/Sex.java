package com.ag04.moneyheist.domain;

/**
 * Enumerates member's sex
 *
 * @author dlokner
 */
public enum Sex {
    M,
    F
}
