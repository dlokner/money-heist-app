package com.ag04.moneyheist.domain;

/**
 * {@code Heist} status enumerations.
 *
 * @author dlokner
 */
public enum HeistStatus {
    IN_PROGRESS,
    PLANNING,
    READY,
    FINISHED
}
