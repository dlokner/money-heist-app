package com.ag04.moneyheist.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * JavaBean domain model representing a heist.
 *
 * @author dlokner
 */
@Entity
@Data
@EqualsAndHashCode(exclude = {"members"})
public class Heist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String location;

    private Instant startTime;

    private Instant endTime;

    @OneToMany(mappedBy = "heist", cascade = CascadeType.ALL)
    private Set<HeistSkill> skills = new HashSet<>();

    @Enumerated(value = EnumType.STRING)
    private HeistStatus status = HeistStatus.PLANNING;

    @ManyToMany
    @JoinTable(
            name = "heist_members",
            joinColumns = @JoinColumn(name = "heist_id"),
            inverseJoinColumns = @JoinColumn(name = "member_id")
    )
    @JsonIgnore
    @ToString.Exclude
    private Set<Member> members;
}