package com.ag04.moneyheist.domain;

/**
 * Heist outcome enumerations
 *
 * @author dlokner
 */
public enum Outcome {
    FAILED,
    SUCCEEDED
}
