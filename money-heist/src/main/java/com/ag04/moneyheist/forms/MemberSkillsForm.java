package com.ag04.moneyheist.forms;

import com.ag04.moneyheist.dto.MemberSkillDto;
import lombok.Data;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

/**
 * Form for updating {@code Member} skills.
 *
 * @author dlokner
 */
@Data
public class MemberSkillsForm {

    @Valid
    private List<MemberSkillDto> skills = new LinkedList<>();

    private String mainSkill;
}
