package com.ag04.moneyheist.forms;

import com.ag04.moneyheist.dto.HeistSkillDto;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * Form for updating {@code Heist} skills.
 *
 * @author dlokner
 */
@Data
public class HeistSkillsForm {
    private List<HeistSkillDto> skills = new LinkedList<>();
}
