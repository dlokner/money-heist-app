package com.ag04.moneyheist.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * {@code Heist} member confirm form.
 *
 * @author dlokner
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeistMembersForm {
    public Collection<String> members;
}
