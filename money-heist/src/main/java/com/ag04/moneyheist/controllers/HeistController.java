package com.ag04.moneyheist.controllers;

import com.ag04.moneyheist.dto.*;
import com.ag04.moneyheist.forms.HeistMembersForm;
import com.ag04.moneyheist.forms.HeistSkillsForm;
import com.ag04.moneyheist.services.HeistService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * REST controller for {@code Heist} resources.
 *
 * @see com.ag04.moneyheist.domain.Heist
 * @author dlokner
 */
@RestController
@RequestMapping("/heist")
public class HeistController {

    private final HeistService heistService;

    public HeistController(HeistService heistService) {
        this.heistService = heistService;
    }

    @PostMapping
    public ResponseEntity<Void> postHeist(@Valid @RequestBody HeistDto heistDto) {
        HeistDto newHeist = heistService.createNewHeist(heistDto);
        return ResponseEntity.noContent()
                .headers(createHeader("Location", String.format("/heist/%d", newHeist.getId())))
                .build();
    }

    @PatchMapping("/{id}/skills")
    public ResponseEntity<Void> patchSkills(@PathVariable Long id, @Valid @RequestBody HeistSkillsForm form) {
        heistService.updateHeistSkills(id, form);
        return ResponseEntity.noContent()
                .headers(createHeader("Content-Location", String.format("/heist/%d/skills", id)))
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<HeistDto> getHeist(@PathVariable Long id) {
        HeistDto heistDto = heistService.getHeist(id);
        return new ResponseEntity<>(heistDto, HttpStatus.OK);
    }

    @GetMapping("/{id}/skills")
    public ResponseEntity<Collection<HeistSkillDto>> getSkills(@PathVariable Long id) {
        SkillsCollectionDto<HeistSkillDto> skills = heistService.getSkills(id);
        return new ResponseEntity<>(skills.getSkills(), HttpStatus.OK);
    }

    @GetMapping("/{id}/status")
    public ResponseEntity<HeistStatusDto> getStatus(@PathVariable Long id) {
        HeistStatusDto statusDto = heistService.getStatus(id);
        return new ResponseEntity<>(statusDto, HttpStatus.OK);
    }

    @PutMapping("/{id}/start")
    public ResponseEntity<Void> startHeist(@PathVariable Long id) {
        heistService.startHeist(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}/eligible_members")
    public ResponseEntity<EligibleMembersDto> getEligibleMembers(@PathVariable("id") Long heistId) {
        EligibleMembersDto eligibleMembers = heistService.getEligibleMembers(heistId);
        return new ResponseEntity<>(eligibleMembers, HttpStatus.OK);
    }

    @PutMapping("/{id}/members")
    public ResponseEntity<Void> putHeistMembers(@PathVariable("id") Long heistId, @RequestBody HeistMembersForm form) {
        heistService.setHeistMembers(heistId, form);
        return ResponseEntity.noContent()
                .headers(createHeader("Content-Location", String.format("/heist/%d/members", heistId)))
                .build();
    }

    @GetMapping("/{id}/members")
    public ResponseEntity<Collection<EligibleMemberDto>> getHeistMembers(@PathVariable("id") Long heistId) {
        Collection<EligibleMemberDto> members = heistService.getHeistMembers(heistId);
        return ResponseEntity.ok(members);
    }

    @GetMapping("/{id}/outcome")
    public ResponseEntity<OutcomeDto> getOutcome(@PathVariable Long id) {
        OutcomeDto outcomeDto = heistService.calcHeistOutcome(id);
        return ResponseEntity.ok(outcomeDto);
    }

    private HttpHeaders createHeader(String key, String value) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(key, value);
        return headers;
    }
}
