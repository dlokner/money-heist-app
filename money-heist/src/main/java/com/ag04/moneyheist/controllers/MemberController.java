package com.ag04.moneyheist.controllers;

import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.dto.MemberSkillsCollectionDto;
import com.ag04.moneyheist.forms.MemberSkillsForm;
import com.ag04.moneyheist.services.MemberService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * REST controller for {@code Member} resources.
 *
 * @see com.ag04.moneyheist.domain.Member
 * @author dlokner
 */
@RestController
@RequestMapping("/member")
public class MemberController {

    private final MemberService memberService;

    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @PostMapping
    public ResponseEntity<Void> postMember(@Valid @RequestBody MemberDto memberDto) {
        MemberDto newMember = memberService.addNewMember(memberDto);
        return ResponseEntity
                .noContent()
                .headers(postMemberHeader(newMember.getId()))
                .build();
    }

    private HttpHeaders postMemberHeader(Long memberId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Location", String.format("/member/%d", memberId));
        return headers;
    }

    @PutMapping("/{id}/skills")
    public ResponseEntity<Void> putSkills(@PathVariable Long id, @Valid @RequestBody MemberSkillsForm form) {
        memberService.updateMemberSkills(id, form);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}/skills/{skill_name}")
    public ResponseEntity<Void> deleteSkill(@PathVariable Long id, @PathVariable("skill_name") String name) {
        memberService.deleteMemberSkill(id, name);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<MemberDto> getMember(@PathVariable Long id) {
        MemberDto memberDto = memberService.getMember(id);
        return new ResponseEntity<>(memberDto, HttpStatus.OK);
    }

    @GetMapping("/{id}/skills")
    public ResponseEntity<MemberSkillsCollectionDto<MemberSkillDto>> getSkills(@PathVariable Long id) {
        MemberSkillsCollectionDto<MemberSkillDto> skills = memberService.getSkills(id);
        return new ResponseEntity<>(skills, HttpStatus.OK);
    }

    //
    //  Error handlers
    //

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(
                (error) -> errors.put(((FieldError) error).getField(), error.getDefaultMessage())
        );
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NullPointerException.class)
    public String handleNullPointerException() {
        return "Argument must not be null";
    }
}
