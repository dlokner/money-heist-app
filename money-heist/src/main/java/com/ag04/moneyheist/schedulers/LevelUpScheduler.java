package com.ag04.moneyheist.schedulers;

import com.ag04.moneyheist.domain.HeistStatus;
import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.SkillDto;
import com.ag04.moneyheist.services.HeistService;
import com.ag04.moneyheist.services.MemberSkillService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Scheduler that updates member skill level.
 * Heist required skills levels will be incremented every {@code levelUpTime} secondes.
 *
 * @author dlokner
 */
@Component
public class LevelUpScheduler {

    private final HeistService heistService;
    private final MemberSkillService memberSkillService;

    public LevelUpScheduler(HeistService heistService, MemberSkillService memberSkillService) {
        this.heistService = heistService;
        this.memberSkillService = memberSkillService;
    }

    @Scheduled(fixedRateString = "${levelUpTime}")
    public void levelUpSkills() {
        Collection<HeistDto> inProgress = heistService.getHeistsByStatus(HeistStatus.IN_PROGRESS);
        for (HeistDto h : inProgress) {
            Collection<MemberDto> members = h.getMembers();
            for (MemberDto m : members) {
                upgradeMemberSkills(h.getSkills(), m);
            }
        }
    }

    private void upgradeMemberSkills(Collection<? extends SkillDto> heistSkills, MemberDto m) {
        Set<String> skills = heistSkills.stream().map(SkillDto::getName).collect(Collectors.toSet());
        m.getSkills().stream()
                .filter(s -> skills.contains(s.getName()))
                .forEach(s -> memberSkillService.updateSkillLevel(
                        m.getId(), s.getName(),
                        s.getLevel().length() < 10 ? (s.getLevel().length() + 1) : 10));
    }

}
