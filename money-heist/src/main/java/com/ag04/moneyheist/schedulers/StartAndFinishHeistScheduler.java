package com.ag04.moneyheist.schedulers;

import com.ag04.moneyheist.domain.HeistStatus;
import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.services.HeistService;
import com.ag04.moneyheist.util.HeistUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Collection;

/**
 * This component will automatically start and finish {@code Heist}
 *
 * @author dlokner
 */
@Component
public class StartAndFinishHeistScheduler {

    private final HeistService heistService;

    public StartAndFinishHeistScheduler(HeistService heistService) {
        this.heistService = heistService;
    }

    @Scheduled(fixedRateString = "${startAndFinishRate}")
    public void startAndFinisHeists() {
        Collection<HeistDto> ready = heistService.getHeistsByStatus(HeistStatus.READY);
        Collection<HeistDto> inProgress = heistService.getHeistsByStatus(HeistStatus.IN_PROGRESS);

        Instant now = Instant.now();

        startHeists(ready, now);
        finishHeists(inProgress, now);
    }

    public void startHeists(Collection<HeistDto> ready, Instant now) {
        ready.stream()
                .filter(h -> HeistUtil.canBeStarted(h, now))
                .forEach(h -> heistService.startHeist(h.getId()));
    }

    public void finishHeists(Collection<HeistDto> inProgress, Instant now) {
        inProgress.stream()
                .filter(h -> HeistUtil.canBeFinished(h, now))
                .forEach(h -> heistService.finishHeist(h.getId()));
    }
}
