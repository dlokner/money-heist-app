package com.ag04.moneyheist.repositories;

import com.ag04.moneyheist.domain.HeistSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * {@code HeistSkill} domain objects repository.
 *
 * @author dlokner
 */
@Repository
public interface HeistSkillRepository extends JpaRepository<HeistSkill, Long> {

    /**
     * Deletes all skills with {@code id} in {@code ids} collection.
     *
     * @param ids collection of IDs
     */
    void deleteByIdIn(Collection<Long> ids);

    /**
     * Deletes all skills associated with single {@code Heist} which names are present in given collection
     *
     * @param heistId Heist ID
     * @param names skill names collection
     */
    @Modifying
    @Query("delete from HeistSkill s where s.heist.id = ?1 and s.name in (?2)")
    void deleteByHeistIdAndNameIsIn(Long heistId, Collection<String> names);

    /**
     * Deletes skill associated with specific {@code Heist} and skill {@code name}.
     *
     * @param heistId heist ID
     * @param name name of the skill
     */
    void deleteByHeistIdAndName(Long heistId, String name);

    /**
     * Checks if skill exists in repository.
     *
     * @param id Heist ID
     * @param name skill name
     * @return {@code true} if skill is present, {@code false} otherwise
     */
    boolean existsByHeistIdAndName(Long id, String name);
}
