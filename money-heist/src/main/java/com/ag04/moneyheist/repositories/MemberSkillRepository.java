package com.ag04.moneyheist.repositories;

import com.ag04.moneyheist.domain.MemberSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * {@code MemberSkill} domain objects repository.
 *
 * @author dlokner
 */
@Repository
public interface MemberSkillRepository extends JpaRepository<MemberSkill, Long> {

    /**
     * Deletes all skills with {@code id} in {@code ids} collection.
     *
     * @param ids collection of IDs
     */
    void deleteByIdIn(Collection<Long> ids);

    /**
     * Deletes all skills by single owner which names are present in given collection
     *
     * @param ownerId member ID
     * @param names skill names collection
     */
    @Modifying
    @Query("delete from MemberSkill s where s.owner.id = ?1 and s.name in (?2)")
    void deleteByOwnerIdAndNameIn(Long ownerId, Collection<String> names);

    /**
     * Deletes skill associated with {@code Member} as skill owner and {@code name}.
     *
     * @param ownerId member ID
     * @param name name of the skill
     */
    void deleteByOwnerIdAndName(Long ownerId, String name);

    /**
     * Checks if skill exists in repository.
     *
     * @param id member ID
     * @param name skill name
     * @return {@code true} if skill is present, {@code false} otherwise
     */
    boolean existsByOwnerIdAndName(Long id, String name);
}
