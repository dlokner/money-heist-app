package com.ag04.moneyheist.repositories;

import com.ag04.moneyheist.domain.Heist;
import com.ag04.moneyheist.domain.HeistStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.Collection;

/**
 * {@code Heist} domain objects repository.
 *
 * @author dlokner
 */
public interface HeistRepository extends JpaRepository<Heist, Long> {

    /**
     * Checks if {@code Heist} with given name exists.
     *
     * @param name {@code Heist} name
     * @return {@code true} if heist with given name exists, {@code false} otherwise
     */
    boolean existsByName(String name);

    /**
     * Gets {@code Heist} objects by given status
     *
     * @param status Heist status
     * @retuen collection of heists wit given status
     */
    Collection<Heist> getAllByStatus(HeistStatus status);

    /**
     * Finds all {@code Heist} not in the given interval
     *
     * @param start begining of the interval
     * @param end end of the interval
     * @return all heists out of the given interval
     */
    @Query("select h from Heist h where h.startTime <= ?2 and h.endTime >= ?1")
    Collection<Heist> findOutOfInterval(Instant start, Instant end);
}
