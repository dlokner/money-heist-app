package com.ag04.moneyheist.repositories;

import com.ag04.moneyheist.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * {@code Member} domain objects repository.
 *
 * @author dlokner
 */
@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

    /**
     * Checks if member with given {@code email} exists.
     *
     * @param email email of member whose existence will be checked
     * @return {@code true} if member exists, otherwise {@code false}
     */
    boolean existsByEmail(String email);

    /**
     * Finds all {@code Members} which names are contained in given collection
     *
     * @param names collection of names
     * @return {@code Members} collection
     */
    @Query("select m from Member m where m.name in (?1)")
    Collection<Member> findByNames(Collection<String> names);
}
