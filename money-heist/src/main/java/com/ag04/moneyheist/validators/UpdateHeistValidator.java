package com.ag04.moneyheist.validators;

import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.HeistSkillDto;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Validator for {@code HeistDto} objects that should be updated in repository.
 *
 * @author dlokner
 */
@Component
public class UpdateHeistValidator implements Validator<HeistDto> {

    @Override
    public Map<String, String> validate(HeistDto object) {
        Map<String, String> errors = new HashMap<>();
        Instant startTime = Instant.parse(object.getStartTime());
        Instant endTime = Instant.parse(object.getEndTime());
        if (startTime.compareTo(endTime) > 0) {
            errors.put("timeInterval", "startTime is after the endTime");
        }
        if (multipleEquivalentSkills(object.getSkills())) {
            errors.put("multipleEqSkills", "two skills with same name and same level are not allowed");
        }
        return errors;
    }

    private boolean multipleEquivalentSkills(List<HeistSkillDto> skills) {
        int n = skills.size();
        for (int i = 0; i < n-1; ++i) {
            for (int j = i + 1; j < n; ++j) {
                HeistSkillDto s1 = skills.get(i);
                HeistSkillDto s2 = skills.get(j);
                if (s1.getName().equals(s2.getName()) && s1.getLevel().equals(s2.getLevel())) {
                    return true;
                }
            }
        }
        return false;
    }
}
