package com.ag04.moneyheist.validators;


import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.forms.MemberSkillsForm;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Object that validates {@code SkillsForm} object.
 *
 * @author dlokner
 */
@Component
public class MemberSkillsFormValidator implements Validator<MemberSkillsForm> {

    @Override
    public Map<String, String> validate(MemberSkillsForm object) {
        Map<String, String> errors = new HashMap<>();
        if (!object.getSkills().stream().map(MemberSkillDto::getName).allMatch(new HashSet<>()::add)) {
            errors.put("skills", "Two skills with same name are not allowed.");
        }
        return errors;
    }
}
