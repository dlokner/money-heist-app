package com.ag04.moneyheist.validators;

import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

/**
 * Object that validates {@code MemberDto} object that represents {@code Member} object which should be updated.
 *
 * @author dlokner
 */
@Component
public class UpdateMemberValidator implements Validator<MemberDto> {

    @Override
    public Map<String, String> validate(MemberDto object) {
        Map<String, String> errors = new HashMap<>();
        if (!Objects.isNull(object.getMainSkill()) &&
                object.getSkills().stream()
                        .noneMatch(m -> m.getName().equals(object.getMainSkill()))) {
            errors.put("mainSkill", "Main skill must reference single skill from skills array");
        }
        if (!object.getSkills().stream().map(MemberSkillDto::getName).allMatch(new HashSet<>()::add)) {
            errors.put("skills", "Two skills with same name are not allowed.");
        }
        return errors;
    }
}
