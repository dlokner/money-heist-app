package com.ag04.moneyheist.validators;

import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.repositories.MemberRepository;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Object that validates {@code MemberDto} object that represents {@code Member} object which should be added to repository.
 *
 * @author dlokner
 */
@Component
public class NewMemberValidator implements Validator<MemberDto> {

    private final MemberRepository memberRepository;

    private final UpdateMemberValidator updateMemberValidator;

    public NewMemberValidator(MemberRepository memberRepository, UpdateMemberValidator updateMemberValidator) {
        this.memberRepository = memberRepository;
        this.updateMemberValidator = updateMemberValidator;
    }

    @Override
    public Map<String, String> validate(MemberDto object) {
        Map<String, String> errors = updateMemberValidator.validate(object);
        if (memberRepository.existsByEmail(object.getEmail())) {
            errors.put("email", "Member with email already exists. Email: " + object.getEmail());
        }
        return errors;
    }
}
