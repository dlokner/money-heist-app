package com.ag04.moneyheist.validators;

import java.util.Map;

/**
 * Interface used for object validators.
 *
 * @author dlokner
 */
public interface Validator<T> {

    /**
     * Object validation method.
     *
     * @param object object to be validated
     * @return {@code Map} in which error messages will be saved by specific error keys
     */
    Map<String, String> validate(T object);
}
