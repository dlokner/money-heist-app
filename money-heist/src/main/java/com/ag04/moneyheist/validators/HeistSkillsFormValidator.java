package com.ag04.moneyheist.validators;


import com.ag04.moneyheist.dto.HeistSkillDto;
import com.ag04.moneyheist.forms.HeistSkillsForm;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Object that validates {@code HeistSkillsForm} object.
 *
 * @author dlokner
 */
@Component
public class HeistSkillsFormValidator implements Validator<HeistSkillsForm> {

    @Override
    public Map<String, String> validate(HeistSkillsForm object) {
        Map<String, String> errors = new HashMap<>();
        if (multipleEquivalentSkills(object.getSkills())) {
            errors.put("multipleEqSkills", "two skills with same name and same level are not allowed");
        }
        return errors;
    }

    private boolean multipleEquivalentSkills(List<HeistSkillDto> skills) {
        int n = skills.size();
        for (int i = 0; i < n-1; ++i) {
            for (int j = i + 1; j < n; ++j) {
                HeistSkillDto s1 = skills.get(i);
                HeistSkillDto s2 = skills.get(j);
                if (s1.getName().equals(s2.getName()) && s1.getLevel().equals(s2.getLevel())) {
                    return true;
                }
            }
        }
        return false;
    }
}
