package com.ag04.moneyheist.validators;

import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.repositories.HeistRepository;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Validator for {@code Heist} objects that will be created in repository.
 *
 * @author dlokner
 */
@Component
public class NewHeistValidator implements Validator<HeistDto> {

    private final UpdateHeistValidator updateHeistValidator;

    private final HeistRepository heistRepository;

    public NewHeistValidator(UpdateHeistValidator updateHeistValidator, HeistRepository heistRepository) {
        this.updateHeistValidator = updateHeistValidator;
        this.heistRepository = heistRepository;
    }

    @Override
    public Map<String, String> validate(HeistDto object) {
        Map<String, String> errors = updateHeistValidator.validate(object);
        if (heistRepository.existsByName(object.getName())) {
            errors.put("name", "heist with name " + object.getName() + " already exists");
        }
//        if (!HeistStatus.PLANNING.name().equals(object.getStatus())) {
//            errors.put("status", "new heist status must be set to PLANNING");
//        }
        return errors;
    }
}
