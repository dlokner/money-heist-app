package com.ag04.moneyheist.services.impl;

import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.services.EmailService;
import com.ag04.moneyheist.services.HeistService;
import com.ag04.moneyheist.services.MemberService;
import com.ag04.moneyheist.services.impl.listeners.HeistListener;
import com.ag04.moneyheist.services.impl.listeners.MemberListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class EmailServiceImpl implements EmailService, HeistListener, MemberListener {

    @Value("${mail.address}")
    private String myEmailAddress;

    private final JavaMailSender emailSender;
    private final MemberService memberService;
    private final HeistService heistService;

    public EmailServiceImpl(JavaMailSender emailSender, MemberService memberService, HeistService heistService) {
        this.emailSender = emailSender;
        this.memberService = memberService;
        this.heistService = heistService;
        this.memberService.addListener(this);
        this.heistService.addListener(this);
    }

    @Override
    public void participantMembers(Collection<MemberDto> participants, HeistDto heistDto) {
        for (MemberDto m : participants) {
            sendMessage(m.getEmail(), "New Heist", String.format("Heist confirmed! Heist name: %s", heistDto.getName()));
        }
    }

    @Override
    public void started(HeistDto heist) {
        for (MemberDto m : heist.getMembers()) {
            sendMessage(m.getEmail(), "Heist started", String.format("Heist started! Heist name: %s", heist.getName()));
        }
    }

    @Override
    public void ended(HeistDto heist) {
        for (MemberDto m : heist.getMembers()) {
            sendMessage(m.getEmail(), "Heist Finished", String.format("Heist finished! Heist name: %s", heist.getName()));
        }
    }

    @Override
    public void newMember(MemberDto newMember) {
        sendMessage(newMember.getEmail(), "New Member", "You are now one of the Money Heist App members. Congratulations!");
    }

    @Override
    public void sendMessage(String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(myEmailAddress);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        emailSender.send(message);
    }
}
