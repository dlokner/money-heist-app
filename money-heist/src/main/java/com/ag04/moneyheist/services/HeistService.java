package com.ag04.moneyheist.services;

import com.ag04.moneyheist.domain.HeistStatus;
import com.ag04.moneyheist.dto.*;
import com.ag04.moneyheist.exceptions.MethodNotAllowedException;
import com.ag04.moneyheist.forms.HeistMembersForm;
import com.ag04.moneyheist.forms.HeistSkillsForm;
import com.ag04.moneyheist.services.impl.listeners.HeistListener;

import java.util.Collection;

/**
 * Service processing operations on {@code Heist} resources.
 *
 * @see com.ag04.moneyheist.domain.Heist
 * @author dlokner
 */
public interface HeistService {

    /**
     * Creates new {@code Heist} object.
     *
     * @param heistDto DTO object that represents {@code Heist} which should be created
     * @return new heist added to repository
     * @throws NullPointerException if {@code null} is given as argument
     */
    HeistDto createNewHeist(HeistDto heistDto);

    /**
     * Updates {@code Heist} represented with given DTO.
     *
     * @param heist {@code Heist}
     * @return DTO of updated {@code Heist}
     * @throws NullPointerException if {@code null} is given as argument
     */
    HeistDto updateHeist(HeistDto heist);

    /**
     * Adds or updates skills for {@code Heist} with id {@code heistId}.
     *
     * @param heistId Heist ID
     * @param form Skills to be added or updated
     * @return updated {@code Heist} object
     * @throws com.ag04.moneyheist.exceptions.BadRequestException when multiple skills with same level were provided
     * @throws com.ag04.moneyheist.exceptions.NotFoundException when heist does not exists
     * @throws MethodNotAllowedException when method is called on {@code Heist} that already started
     * @throws NullPointerException if {@code null} is given as any of arguments
     */
    HeistDto updateHeistSkills(Long heistId, HeistSkillsForm form);

    /**
     * Gets {@code Heist} by given {@code id}
     *
     * @param id Heist ID
     * @return DTO for requested {@code Heist}
     * @throws MethodNotAllowedException when method is called on {@code Heist} that already started
     * @throws NullPointerException if {@code null} is given as argument
     */
    HeistDto getHeist(Long id);

    /**
     * Fetches all {@code Heist} resources
     *
     * @return collection containing all {@code Heist} resources
     */
    Collection<HeistDto> getAllHeists();

    /**
     * Gets skills required by {@code Heist}.
     *
     * @param id Heist ID
     * @return Collection of skills
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} with given {@code id} is not found
     */
    SkillsCollectionDto<HeistSkillDto> getSkills(Long id);

    /**
     * Gets {@code Heist} status.
     * @param id Heist ID
     * @return {@code Heist} status
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} with given {@code id} is not found
     */
    HeistStatusDto getStatus(Long id);

    /**
     * Gets all members eligible for requested {@code Heist}
     *
     * @param id Heist ID
     * @return Eligible members
     * @throws if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} with given {@code id} is not found
     * @throws MethodNotAllowedException if {@code Heist} members are already confirmed
     */
    EligibleMembersDto getEligibleMembers(Long id);

    /**
     * Changes {@code Heist} status to {@code IN_PROGRESS}
     *
     * @param id Heist ID
     * @return DTO for updated {@code Heist}
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} with given {@code id} is not found
     * @throws MethodNotAllowedException if {@code Heist} current status is not {@code READY}
     */
    HeistDto startHeist(Long id);

    /**
     * Changes {@code Heist} status to {@code FINISHED}
     *
     * @param id Heist ID
     * @return DTO for updated {@code Heist}
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} with given {@code id} is not found
     * @throws MethodNotAllowedException if {@code Heist} current status is not {@code IN_PROGRESS}
     */
    HeistDto finishHeist(Long id);

    /**
     * Sets {@code Members} that should participate the heist.
     *
     * @param heistId Heist ID
     * @param form Heist members to be confirmed as participants
     * @throws com.ag04.moneyheist.exceptions.BadRequestException when member does not exist or
     *                                                            any of members is not eligible depending on skills or heist time window
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} with given {@code heistId} is not found
     * @throws MethodNotAllowedException when heist {@code status} is not {@code PLANNING}
     * @return DTO for updated {@code Heist}
     */
    HeistDto setHeistMembers(Long heistId, HeistMembersForm form);

    /**
     * Get all heists with given status.
     *
     * @param status Heist status
     * @return collection of the {@code Heists} associated by given {@code status}
     * @throws NullPointerException if {@code null} is given as argument
     */
    Collection<HeistDto> getHeistsByStatus(HeistStatus status);

    /**
     * Calculates outcome of the {@code Heist}.
     *
     * @param id Heist ID
     * @return Heist outcome
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Heist} does not exist
     * @throws MethodNotAllowedException when {@code Heist} status is not {@code FINISHED}
     */
    OutcomeDto calcHeistOutcome(Long id);

    /**
     * Registers new {@code HeistListener}
     *
     * @param l listener to be registered
     */
    void addListener(HeistListener l);

    /**
     * Removes new {@code HeistListener}
     *
     * @param l listener to be removed
     */
    void removeListener(HeistListener l);

    /**
     * Gets confirmed {@code Heist} members
     *
     * @param heistId Heist ID
     * @return {@code Members} confirmed to {@code Heist}
     */
    Collection<EligibleMemberDto> getHeistMembers(Long heistId);
}
