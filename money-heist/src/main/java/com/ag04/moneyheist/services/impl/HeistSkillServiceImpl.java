package com.ag04.moneyheist.services.impl;

import com.ag04.moneyheist.repositories.HeistSkillRepository;
import com.ag04.moneyheist.services.HeistSkillService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class HeistSkillServiceImpl implements HeistSkillService {

    private final HeistSkillRepository heistSkillRepository;

    public HeistSkillServiceImpl(HeistSkillRepository heistSkillRepository) {
        this.heistSkillRepository = heistSkillRepository;
    }

    @Override
    public void deleteSkills(Long heistId, Collection<String> names) {
        if (!names.isEmpty()) {
            heistSkillRepository.deleteByHeistIdAndNameIsIn(heistId, names);
        }
    }

    @Override
    public void deleteSkill(Long heistId, String name) {
        heistSkillRepository.deleteByHeistIdAndName(heistId, name);
    }

    @Override
    public boolean existsByName(Long id, String name) {
        return heistSkillRepository.existsByHeistIdAndName(id, name);
    }
}
