package com.ag04.moneyheist.services;

import java.util.Collection;

/**
 * Service processing operations on {@code HeistSkill} resources.
 *
 * @see com.ag04.moneyheist.domain.HeistSkill
 * @author dlokner
 */
public interface HeistSkillService {

    /**
     * Delete all skills associated by {@code Heist} with {@code heistId} which names are present in given collection
     *
     * @param heistId ID of member
     * @param names skill names collection
     */
    void deleteSkills(Long heistId, Collection<String> names);

    /**
     * Deletes skill associated with {@code Heist} and skill {@code name}.
     *
     * @param heistId member ID
     * @param name name of the skill
     */
    void deleteSkill(Long heistId, String name);

    /**
     * Checks if skill is present in repository.
     *
     * @param id heist ID
     * @param name skill name
     * @return {@code true} if skill is present, {@code false} otherwise
     */
    boolean existsByName(Long id, String name);
}
