package com.ag04.moneyheist.services.impl;

import com.ag04.moneyheist.converters.MemberDtoToMember;
import com.ag04.moneyheist.converters.MemberDtoToMembersSkillsCollectionDto;
import com.ag04.moneyheist.converters.MemberToMemberDto;
import com.ag04.moneyheist.domain.Member;
import com.ag04.moneyheist.domain.Status;
import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.dto.MemberSkillsCollectionDto;
import com.ag04.moneyheist.exceptions.BadRequestException;
import com.ag04.moneyheist.exceptions.NotFoundException;
import com.ag04.moneyheist.forms.MemberSkillsForm;
import com.ag04.moneyheist.repositories.MemberRepository;
import com.ag04.moneyheist.services.MemberService;
import com.ag04.moneyheist.services.MemberSkillService;
import com.ag04.moneyheist.services.impl.listeners.MemberListener;
import com.ag04.moneyheist.validators.MemberSkillsFormValidator;
import com.ag04.moneyheist.validators.NewMemberValidator;
import com.ag04.moneyheist.validators.UpdateMemberValidator;
import com.ag04.moneyheist.validators.Validator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static com.ag04.moneyheist.util.HeistUtil.checkForBadRequest;
import static com.ag04.moneyheist.util.HeistUtil.redundantSkills;

@Service
public class MemberServiceImpl implements MemberService {

    private final MemberRepository memberRepository;

    private final NewMemberValidator newMemberValidator;
    private final MemberSkillsFormValidator memberSkillsFormValidator;
    private final UpdateMemberValidator updateMemberValidator;

    private final MemberDtoToMember memberDtoToMember;
    private final MemberToMemberDto memberToMemberDto;
    private final MemberDtoToMembersSkillsCollectionDto memberDtoToMembersSkillsCollectionDto;

    private final MemberSkillService memberSkillService;

    private final List<MemberListener> listeners = new LinkedList<>();

    public MemberServiceImpl(MemberRepository memberRepository, NewMemberValidator newMemberValidator,
                             MemberSkillsFormValidator memberSkillsFormValidator,
                             UpdateMemberValidator updateMemberValidator, MemberDtoToMember memberDtoToMember, MemberToMemberDto memberToMemberDto,
                             MemberDtoToMembersSkillsCollectionDto memberDtoToMembersSkillsCollectionDto, MemberSkillService memberSkillService) {
        this.memberRepository = memberRepository;
        this.newMemberValidator = newMemberValidator;
        this.memberSkillsFormValidator = memberSkillsFormValidator;
        this.updateMemberValidator = updateMemberValidator;
        this.memberDtoToMember = memberDtoToMember;
        this.memberToMemberDto = memberToMemberDto;
        this.memberDtoToMembersSkillsCollectionDto = memberDtoToMembersSkillsCollectionDto;
        this.memberSkillService = memberSkillService;
    }


    @Override
    @Transactional
    public MemberDto addNewMember(MemberDto memberDto) {
        Objects.requireNonNull(memberDto);
        MemberDto savedMember = this.saveMember(memberDto, newMemberValidator);
        notifyNewMember(savedMember);
        return savedMember;
    }

    @Override
    @Transactional
    public MemberDto updateMember(MemberDto memberDto) {
        Objects.requireNonNull(memberDto);
        if (!memberRepository.existsById(memberDto.getId())) {
            throw new BadRequestException(String.format("Heist with ID: %d is not found.", memberDto.getId()));
        }
        return this.saveMember(memberDto, updateMemberValidator);
    }

    @Override
    @Transactional
    public MemberDto updateMemberSkills(Long id, MemberSkillsForm form) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(form);

        Optional<Member> optionalMember = memberRepository.findById(id);

        if (optionalMember.isEmpty()) {
            throw new NotFoundException(String.format("Member with ID=%d is not found in repository.", id));
        }
        checkForBadRequest(form, memberSkillsFormValidator);

        MemberDto oldMemberDto = memberToMemberDto.convert(optionalMember.get());
        return updateSkills(oldMemberDto, form);
    }

    @Override
    @Transactional
    public void deleteMemberSkill(Long memberId, String name) {
        Objects.requireNonNull(memberId);
        Objects.requireNonNull(name);

        Optional<Member> optionalMember = memberRepository.findById(memberId);
        if (optionalMember.isEmpty()) {
            throw new NotFoundException(String.format("Member with ID=%d is not found in repository.", memberId));
        }
        if (!memberSkillService.existsByName(memberId, name)) {
            throw new NotFoundException(String.format("Member with ID:%d doesn't have skill: %s", memberId, name));
        }

        if (name.equals(optionalMember.get().getMainSkill())) {
            MemberDto updatedMember = memberToMemberDto.convert(optionalMember.get());
            updatedMember.getSkills().removeIf(s -> s.getName().equals(name));
            updatedMember.setMainSkill(null);
            updateMember(updatedMember);
        }
        memberSkillService.deleteSkill(memberId, name);
    }

    @Override
    public MemberDto getMember(Long id) {
        Objects.requireNonNull(id);
        Optional<Member> optionalMember = memberRepository.findById(id);
        optionalMember.orElseThrow(() -> new NotFoundException(String.format("Member with ID=%d is not found", id)));
        return memberToMemberDto.convert(optionalMember.get());
    }

    @Override
    public Collection<MemberDto> getMembersByNames(Collection<String> names) {
        Objects.requireNonNull(names);

        return memberRepository.findByNames(names).stream().map(memberToMemberDto::convert).collect(Collectors.toList());
    }

    @Override
    public Collection<MemberDto> getAllMembers() {
        return memberRepository.findAll().stream().map(memberToMemberDto::convert).collect(Collectors.toList());
    }

    @Override
    public MemberSkillsCollectionDto<MemberSkillDto> getSkills(Long id) {
        Objects.requireNonNull(id);
        MemberDto memberDto = getMember(id);
        return memberDtoToMembersSkillsCollectionDto.convert(memberDto);
    }

    @Override
    public MemberDto setStatus(Long id, Status status) {
        Objects.requireNonNull(status);
        MemberDto memberDto = getMember(id);
        memberDto.setStatus(status.name());
        return updateMember(memberDto);
    }

    private MemberDto updateSkills(MemberDto memberDto, MemberSkillsForm form) {
        Collection<String> redundantSkills = redundantSkills(form.getSkills(), memberDto.getSkills());
        memberSkillService.deleteSkills(memberDto.getId(), redundantSkills);

        List<MemberSkillDto> newSkills = memberDto.getSkills().stream()
                .filter(s -> !redundantSkills.contains(s.getName()))
                .collect(Collectors.toList());
        newSkills.addAll(form.getSkills());

        memberDto.setSkills(newSkills);
        if (!Objects.isNull(form.getMainSkill())){
            memberDto.setMainSkill(form.getMainSkill());
        }
        return updateMember(memberDto);
    }

    private MemberDto saveMember(MemberDto memberDto, Validator<MemberDto> validator) {
        Objects.requireNonNull(memberDto);

        checkForBadRequest(memberDto, validator);

        Member newMember = memberDtoToMember.convert(memberDto);
        Member savedMember = memberRepository.save(newMember);
        return memberToMemberDto.convert(savedMember);
    }

    @Override
    public void addListener(MemberListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListener(MemberListener l) {
        listeners.remove(l);
    }

    private void notifyNewMember(MemberDto member) {
        for (MemberListener l : listeners) {
            l.newMember(member);
        }
    }
}
