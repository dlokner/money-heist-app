package com.ag04.moneyheist.services;

import com.ag04.moneyheist.domain.Status;
import com.ag04.moneyheist.dto.MemberDto;
import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.dto.MemberSkillsCollectionDto;
import com.ag04.moneyheist.forms.MemberSkillsForm;
import com.ag04.moneyheist.services.impl.listeners.MemberListener;

import java.util.Collection;

/**
 * Service processing operations on {@code Member} resources.
 *
 * @see com.ag04.moneyheist.domain.Member
 * @author dlokner
 */
public interface MemberService {

    /**
     * Creates new {@code Member} resource.
     *
     * @param memberDto {@code MemberDto} object that represents member to be created
     * @return created member represented with corresponding {@code MemberDto} object
     * @throws com.ag04.moneyheist.exceptions.BadRequestException if any of constraints is violated
     * @throws NullPointerException if {@code null} is given as argument
     */
    MemberDto addNewMember(MemberDto memberDto);

    /**
     * Updates {@code Member} resource.
     *
     * @param memberDto {@code MemberDto} object that represents member update
     * @return created member represented with corresponding {@code MemberDto} object
     * @throws com.ag04.moneyheist.exceptions.BadRequestException if any of constraints is violated
     * @throws NullPointerException if {@code null} is given as argument
     */
    MemberDto updateMember(MemberDto memberDto);

    /**
     * Updates skills of {@code Member} with given {@code id}.
     *
     * @param id {@code Member} ID
     * @param form form with updated skills
     * @return DTO for updated member
     * @throws NullPointerException if any of given arguments is {@code null}.
     * @throws com.ag04.moneyheist.exceptions.BadRequestException if main skill is not part of members skills or
     *                                                            if there are two skills with same name
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if the member does not exist
     */
    MemberDto updateMemberSkills(Long id, MemberSkillsForm form);

    /**
     * Deletes skill by ID.
     *
     * @param memberId  member ID
     * @param name   name of the skill to be removed
     * @throws NullPointerException if any of given arguments is {@code null}.
     * @throws com.ag04.moneyheist.exceptions.BadRequestException when the member or the member's skill does not exist
     */
    void deleteMemberSkill(Long memberId, String name);

    /**
     * Gets {@code Member} resource with given ID
     *
     * @param id Member ID
     * @return member with given {@code id}
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Member} with given {@code id} is not found.
     */
    MemberDto getMember(Long id);

    /**
     * Gets all members which names are found in given collection
     *
     * @param names collection of names
     * @return collection of members with given names
     * @throws NullPointerException if {@code null} is given as argument
     */
    Collection<MemberDto> getMembersByNames(Collection<String> names);

    /**
     * Gets all {@code Member} objects from repository
     *
     * @return all members
     */
    Collection<MemberDto> getAllMembers();

    /**
     * Get skills for requested member
     *
     * @param id Member ID
     * @return Member's skills
     * @throws NullPointerException if {@code null} is given as argument
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if {@code Member} with given {@code id} is not found.
     */
    MemberSkillsCollectionDto<MemberSkillDto> getSkills(Long id);

    /**
     * Sets {@code Member} status
     *
     * @param status status to be set
     * @param id Member ID
     * @return DTO for updated {@code Member}
     * @throws NullPointerException if given argument is {@code null}
     * @throws com.ag04.moneyheist.exceptions.NotFoundException if member is not found
     */
    MemberDto setStatus(Long id, Status status);

    /**
     * Registers new {@code MemberListener}
     *
     * @param l listener to be registered
     */
    void addListener(MemberListener l);

    /**
     * Removes new {@code MemberListener}
     *
     * @param l listener to be removed
     */
    void removeListener(MemberListener l);
}
