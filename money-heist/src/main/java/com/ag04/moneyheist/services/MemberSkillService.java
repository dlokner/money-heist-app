package com.ag04.moneyheist.services;

import com.ag04.moneyheist.dto.MemberSkillDto;

import java.util.Collection;

/**
 * Service processing operations on {@code MemberSkill} resources.
 *
 * @see com.ag04.moneyheist.domain.MemberSkill
 * @author dlokner
 */
public interface MemberSkillService {

    /**
     * Deletes all skills wit IDs in {@code ids} collection.
     *
     * @param ids collection of IDs
     */
    void deleteSkills(Collection<Long> ids);

    /**
     * Delete all skills by single owner which names are present in given collection
     *
     * @param ownerId ID of member
     * @param names skill names collection
     */
    void deleteSkills(Long ownerId, Collection<String> names);

    /**
     * Deletes skill associated with {@code Member} as skill owner and skill {@code name}.
     *
     * @param ownerId member ID
     * @param name name of the skill
     */
    void deleteSkill(Long ownerId, String name);

    /**
     * Checks if skill is present in repository.
     *
     * @param id member ID
     * @param name skill name
     * @return {@code true} if skill is present, {@code false} otherwise
     */
    boolean existsByName(Long id, String name);

    /**
     * Updates skill level
     *
     * @param id member ID
     * @param name skill name
     * @param level new skill level
     * @return DTO of updated {@code HeistSkill}
     */
    MemberSkillDto updateSkillLevel(Long id, String name, int level);
}
