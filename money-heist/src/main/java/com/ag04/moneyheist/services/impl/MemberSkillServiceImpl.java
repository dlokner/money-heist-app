package com.ag04.moneyheist.services.impl;

import com.ag04.moneyheist.converters.MemberSkillToMemberSkillDto;
import com.ag04.moneyheist.domain.MemberSkill;
import com.ag04.moneyheist.dto.MemberSkillDto;
import com.ag04.moneyheist.exceptions.BadRequestException;
import com.ag04.moneyheist.exceptions.NotFoundException;
import com.ag04.moneyheist.repositories.MemberSkillRepository;
import com.ag04.moneyheist.services.MemberSkillService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class MemberSkillServiceImpl implements MemberSkillService {

    private final MemberSkillRepository memberSkillRepository;

    private final MemberSkillToMemberSkillDto memberSkillToMemberSkillDto;

    public MemberSkillServiceImpl(MemberSkillRepository memberSkillRepository, MemberSkillToMemberSkillDto memberSkillToMemberSkillDto) {
        this.memberSkillRepository = memberSkillRepository;
        this.memberSkillToMemberSkillDto = memberSkillToMemberSkillDto;
    }

    @Override
    public void deleteSkills(Collection<Long> ids) {
        memberSkillRepository.deleteByIdIn(ids);
    }

    @Override
    public void deleteSkills(Long ownerId, Collection<String> names) {
        if (!names.isEmpty()) {
            memberSkillRepository.deleteByOwnerIdAndNameIn(ownerId, names);
        }
    }

    @Override
    public void deleteSkill(Long ownerId, String name) {
        memberSkillRepository.deleteByOwnerIdAndName(ownerId, name);
    }

    @Override
    public boolean existsByName(Long id, String name) {
        return memberSkillRepository.existsByOwnerIdAndName(id, name);
    }

    @Override
    public MemberSkillDto updateSkillLevel(Long id, String name, int level) {
        Optional<MemberSkill> optionalHeistSkill = memberSkillRepository.findById(id);
        optionalHeistSkill.orElseThrow(() -> new NotFoundException(name + " - skill is not found"));
        if (level < 1 || level > 10) {
            throw new BadRequestException("Invalid skill level");
        }
        MemberSkill skill = optionalHeistSkill.get();
        skill.setLevel(StringUtils.repeat('*', level));
        return memberSkillToMemberSkillDto.convert(memberSkillRepository.save(skill));
    }
}
