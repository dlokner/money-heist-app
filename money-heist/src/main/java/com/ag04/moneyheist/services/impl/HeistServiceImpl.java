package com.ag04.moneyheist.services.impl;

import com.ag04.moneyheist.converters.HeistDtoToHeist;
import com.ag04.moneyheist.converters.HeistDtoToSkillsCollectionDto;
import com.ag04.moneyheist.converters.HeistToHeistDto;
import com.ag04.moneyheist.converters.MemberDtoToMember;
import com.ag04.moneyheist.domain.Heist;
import com.ag04.moneyheist.domain.HeistStatus;
import com.ag04.moneyheist.domain.Member;
import com.ag04.moneyheist.domain.Outcome;
import com.ag04.moneyheist.dto.*;
import com.ag04.moneyheist.exceptions.BadRequestException;
import com.ag04.moneyheist.exceptions.MethodNotAllowedException;
import com.ag04.moneyheist.exceptions.NotFoundException;
import com.ag04.moneyheist.forms.HeistMembersForm;
import com.ag04.moneyheist.forms.HeistSkillsForm;
import com.ag04.moneyheist.repositories.HeistRepository;
import com.ag04.moneyheist.services.HeistService;
import com.ag04.moneyheist.services.HeistSkillService;
import com.ag04.moneyheist.services.MemberService;
import com.ag04.moneyheist.services.impl.listeners.HeistListener;
import com.ag04.moneyheist.util.EligibleMembersSearch;
import com.ag04.moneyheist.util.HeistOutcomeCalc;
import com.ag04.moneyheist.validators.HeistSkillsFormValidator;
import com.ag04.moneyheist.validators.NewHeistValidator;
import com.ag04.moneyheist.validators.UpdateHeistValidator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static com.ag04.moneyheist.util.HeistUtil.checkForBadRequest;
import static com.ag04.moneyheist.util.HeistUtil.redundantSkills;

@Service
public class HeistServiceImpl implements HeistService {

    private final HeistRepository heistRepository;

    private final HeistDtoToHeist heistDtoToHeist;
    private final HeistToHeistDto heistToHeistDto;
    private final HeistDtoToSkillsCollectionDto heistDtoToSkillsCollectionDto;
    private final MemberDtoToMember memberDtoToMember;

    private final NewHeistValidator newHeistValidator;
    private final UpdateHeistValidator updateHeistValidator;
    private final HeistSkillsFormValidator heistSkillsFormValidator;

    private final HeistSkillService heistSkillService;
    private final MemberService memberService;

    private final EligibleMembersSearch eligibleMembersSearch;
    private final HeistOutcomeCalc heistOutcomeCalc;

    private final List<HeistListener> listeners = new LinkedList<>();

    public HeistServiceImpl(HeistRepository heistRepository,
                            HeistDtoToHeist heistDtoToHeist, HeistToHeistDto heistToHeistDto,
                            HeistDtoToSkillsCollectionDto heistDtoToSkillsCollectionDto, MemberDtoToMember memberDtoToMember, NewHeistValidator newHeistValidator, UpdateHeistValidator updateHeistValidator,
                            HeistSkillsFormValidator heistSkillsFormValidator,
                            HeistSkillService heistSkillService, MemberService memberService,
                            EligibleMembersSearch eligibleMembersSearch, HeistOutcomeCalc heistOutcomeCalc) {
        this.heistRepository = heistRepository;
        this.heistDtoToHeist = heistDtoToHeist;
        this.heistToHeistDto = heistToHeistDto;
        this.heistDtoToSkillsCollectionDto = heistDtoToSkillsCollectionDto;
        this.memberDtoToMember = memberDtoToMember;
        this.newHeistValidator = newHeistValidator;
        this.updateHeistValidator = updateHeistValidator;
        this.heistSkillsFormValidator = heistSkillsFormValidator;
        this.heistSkillService = heistSkillService;
        this.memberService = memberService;
        this.eligibleMembersSearch = eligibleMembersSearch;
        this.heistOutcomeCalc = heistOutcomeCalc;
    }

    @Override
    @Transactional
    public HeistDto createNewHeist(HeistDto heistDto) {
        Objects.requireNonNull(heistDto);

        if (Objects.isNull(heistDto.getStatus())) heistDto.setStatus(HeistStatus.PLANNING.name());

        checkForBadRequest(heistDto, newHeistValidator);

        Heist newHeist = heistDtoToHeist.convert(heistDto);
        Heist saved = heistRepository.save(newHeist);
        return heistToHeistDto.convert(saved);
    }

    @Override
    @Transactional
    public HeistDto updateHeist(HeistDto heistDto) {
        Objects.requireNonNull(heistDto);
        checkForBadRequest(heistDto, updateHeistValidator);

        Heist newHeist = heistDtoToHeist.convert(heistDto);

        if (!heistRepository.existsById(heistDto.getId())) {
            throw new BadRequestException(String.format("Heist with ID: %d is not found.", heistDto.getId()));
        }
        Heist saved = heistRepository.save(newHeist);
        return heistToHeistDto.convert(saved);
    }

    @Override
    @Transactional
    public HeistDto updateHeistSkills(Long heistId, HeistSkillsForm form) {
        Objects.requireNonNull(heistId);
        Objects.requireNonNull(form);

        Optional<Heist> optionalHeist = heistRepository.findById(heistId);
        if (optionalHeist.isEmpty()) {
            throw new NotFoundException(String.format("Heist with ID=%d is not found in repository.", heistId));
        }
        checkForBadRequest(form, heistSkillsFormValidator);

        HeistDto oldHeist = heistToHeistDto.convert(optionalHeist.get());
        return updateSkills(oldHeist, form);
    }

    @Override
    public HeistDto getHeist(Long id) {
        Objects.requireNonNull(id);
        Optional<Heist> optionalHeist = heistRepository.findById(id);
        optionalHeist.orElseThrow(() -> new NotFoundException(String.format("Heist with ID=%d is not found", id)));
        return heistToHeistDto.convert(optionalHeist.get());
    }

    @Override
    public Collection<HeistDto> getAllHeists() {
        Collection<Heist> heists = heistRepository.findAll();
        return heists.stream().map(heistToHeistDto::convert).collect(Collectors.toList());
    }

    @Override
    public SkillsCollectionDto<HeistSkillDto> getSkills(Long id) {
        Objects.requireNonNull(id);
        HeistDto heistDto = getHeist(id);
        return heistDtoToSkillsCollectionDto.convert(heistDto);
    }

    @Override
    public HeistStatusDto getStatus(Long id) {
        Objects.requireNonNull(id);
        HeistDto heistDto = getHeist(id);
        HeistStatusDto statusDto = new HeistStatusDto();
        statusDto.setStatus(heistDto.getStatus());
        return statusDto;
    }

    @Override
    public EligibleMembersDto getEligibleMembers(Long id) {
        Objects.requireNonNull(id);
        Optional<Heist> optionalHeistDto = heistRepository.findById(id);
        optionalHeistDto.orElseThrow(() -> new NotFoundException(String.format("Heist with ID=%d is not found!", id)));
        if (HeistStatus.READY.equals(optionalHeistDto.get().getStatus())) {
            throw new MethodNotAllowedException("Members for the heist are already confirmed");
        }
        HeistDto heistDto = heistToHeistDto.convert(optionalHeistDto.get());
        Collection<EligibleMemberDto> members = eligibleMembersSearch.searchForMembers(heistDto);
        return new EligibleMembersDto(heistDto.getSkills(), members);
    }

    @Override
    @Transactional
    public HeistDto startHeist(Long id) {
        Objects.requireNonNull(id);
        HeistDto heistDto = getHeist(id);
        if (!canBeStarted(heistDto)) {
            throw new MethodNotAllowedException("Heist status must be READY before heist is started!");
        }
        heistDto.setStatus(HeistStatus.IN_PROGRESS.name());
        HeistDto updatedHeist = updateHeist(heistDto);
        notifyStarted(updatedHeist);
        return updatedHeist;
    }

    @Override
    @Transactional
    public HeistDto finishHeist(Long id) {
        Objects.requireNonNull(id);
        HeistDto heistDto = getHeist(id);
        if (!canBeFinished(heistDto)) {
            throw new MethodNotAllowedException("Heist status must be IN_PROGRESS before heist can be finished!");
        }
        heistDto.setStatus(HeistStatus.FINISHED.name());
        HeistDto updatedHeist = updateHeist(heistDto);
        notifyEnded(updatedHeist);
        return updatedHeist;
    }

    @Override
    @Transactional
    public HeistDto setHeistMembers(Long heistId, HeistMembersForm form) {
        Objects.requireNonNull(heistId);
        HeistDto heistDto = getHeist(heistId);
        Collection<MemberDto> membersDto = memberService.getMembersByNames(form.members);
        if (membersDto.size() != form.members.size()) {
            throw new BadRequestException("Some of the members do not exist");
        }
        if (!HeistStatus.PLANNING.equals(HeistStatus.valueOf(heistDto.getStatus()))) {
            throw new MethodNotAllowedException("Heist status must be set to PLANNING before members are confirmed");
        }
        if (membersDto.size() != eligibleMembersSearch.filterEligibleMembers(heistDto, membersDto).size()) {
            throw new BadRequestException("Some members do not have required skills");
        }
        Set<Member> members = membersDto.stream().map(memberDtoToMember::convert).collect(Collectors.toSet());
        Heist newHeist = heistDtoToHeist.convert(heistDto);
        newHeist.setMembers(members);
        newHeist.setStatus(HeistStatus.READY);

        Heist savedHeist = heistRepository.save(newHeist);
        HeistDto savedHeistDto = heistToHeistDto.convert(savedHeist);
        notifyParticipantConfirmed(savedHeistDto.getMembers(), savedHeistDto);
        return savedHeistDto;
    }

    @Override
    @Transactional
    public Collection<EligibleMemberDto> getHeistMembers(Long heistId) {
        Objects.requireNonNull(heistId);
        HeistDto heistDto = getHeist(heistId);
        if (HeistStatus.PLANNING.name().equals(heistDto.getStatus())) {
            throw new MethodNotAllowedException("Heist status is still PLANNING");
        }
        Collection<MemberDto> members = heistDto.getMembers();
        return eligibleMembersSearch.membersDtoToEligibleMembersDto(heistDto, members);
    }

    @Override
    @Transactional
    public Collection<HeistDto> getHeistsByStatus(HeistStatus status) {
        Objects.requireNonNull(status);
        Collection<Heist> heists = heistRepository.getAllByStatus(status);
        return heists.stream().map(heistToHeistDto::convert).collect(Collectors.toSet());
    }

    @Override
    public OutcomeDto calcHeistOutcome(Long id) {
        Objects.requireNonNull(id);
        Optional<Heist> optionalHeist = heistRepository.findById(id);
        optionalHeist.orElseThrow(() -> new NotFoundException(String.format("Heist with ID=%d is not found", id)));
        if (!HeistStatus.FINISHED.equals(optionalHeist.get().getStatus())) {
            throw new MethodNotAllowedException("Heist status must be finished before outcome calculation");
        }
        Outcome outcome = heistOutcomeCalc.calcHeistOutcome(optionalHeist.get());
        return new OutcomeDto(outcome);
    }

    private HeistDto updateSkills(HeistDto heist, HeistSkillsForm form) {
        Collection<String> redundantSkills = redundantSkills(form.getSkills(), heist.getSkills());
        heistSkillService.deleteSkills(heist.getId(), redundantSkills);

        List<HeistSkillDto> newSkills = heist.getSkills().stream()
                .filter(s -> !redundantSkills.contains(s.getName()))
                .collect(Collectors.toList());
        newSkills.addAll(form.getSkills());

        heist.setSkills(newSkills);
        return updateHeist(heist);
    }

    private boolean canBeStarted(HeistDto heist) {
        return HeistStatus.READY.name().equals(heist.getStatus());
    }

    private boolean canBeFinished(HeistDto heist) {
        return HeistStatus.IN_PROGRESS.name().equals(heist.getStatus());
    }

    @Override
    public void addListener(HeistListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListener(HeistListener l) {
        listeners.remove(l);
    }

    private void notifyParticipantConfirmed(Collection<MemberDto> members, HeistDto heistDto) {
        for (HeistListener l : listeners) {
            l.participantMembers(members, heistDto);
        }
    }

    private void notifyStarted(HeistDto heistDto) {
        for (HeistListener l : listeners) {
            l.started(heistDto);
        }
    }

    private void notifyEnded(HeistDto heistDto) {
        for (HeistListener l : listeners) {
            l.ended(heistDto);
        }
    }
}
