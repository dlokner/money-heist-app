package com.ag04.moneyheist.services.impl.listeners;

import com.ag04.moneyheist.dto.MemberDto;

/**
 * Listener on {@code Member} resources
 *
 * @author dlokner
 */
public interface MemberListener {
    void newMember(MemberDto newMember);
}
