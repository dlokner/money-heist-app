package com.ag04.moneyheist.services.impl.listeners;

import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.MemberDto;

import java.util.Collection;

/**
 * Listener on {@code Heist} objects used to notify basic changes in heist.
 *
 * @author dlokner
 */
public interface HeistListener {
    void participantMembers(Collection<MemberDto> participants, HeistDto heistDto);
    void started(HeistDto heist);
    void ended(HeistDto heist);
}
