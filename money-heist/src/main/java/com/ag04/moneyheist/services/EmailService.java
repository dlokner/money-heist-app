package com.ag04.moneyheist.services;

/**
 * Service for E-Mail communication
 *
 * @author dlokner
 */
public interface EmailService {

    /**
     * Sends simple E-Mail message
     *
     * @param to destination E-Mail address
     * @param subject E-Mail message subject
     * @param message message body
     */
    void sendMessage(String to, String subject, String message);
}
