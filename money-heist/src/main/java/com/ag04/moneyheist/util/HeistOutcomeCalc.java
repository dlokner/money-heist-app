package com.ag04.moneyheist.util;

import com.ag04.moneyheist.domain.*;
import com.ag04.moneyheist.exceptions.MethodNotAllowedException;
import com.ag04.moneyheist.services.MemberService;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Class that offers method for setting outcome of the Heist.
 *
 * @author dlokner
 */
@Component
public class HeistOutcomeCalc {

    private final Random random = new Random();

    private final StatusPossibility S1 = new StatusPossibility(1.0, Arrays.asList(Status.EXPIRED, Status.INCARCERATED));
    private final StatusPossibility S2 = new StatusPossibility(2.0/3.0, Arrays.asList(Status.EXPIRED, Status.INCARCERATED));
    private final StatusPossibility S3 = new StatusPossibility(1.0/3.0, Arrays.asList(Status.EXPIRED, Status.INCARCERATED));
    private final StatusPossibility S4 = new StatusPossibility(1.0/3.0, Collections.singletonList(Status.INCARCERATED));
    private final StatusPossibility S5 = new StatusPossibility(0.0, null);

    private final StatusPossibility[] STATUSES = new StatusPossibility[] {S1, S2, S3, S4, S5};
    private final Outcome[] OUTCOMES = new Outcome[] {Outcome.FAILED, Outcome.FAILED, Outcome.SUCCEEDED, Outcome.SUCCEEDED, Outcome.SUCCEEDED};

    private final MemberService memberService;

    public HeistOutcomeCalc(MemberService memberService) {
        this.memberService = memberService;
    }

    public Outcome calcHeistOutcome(Heist heist) {
        Objects.requireNonNull(heist);
        if (!HeistStatus.FINISHED.equals(heist.getStatus())) {
            throw new MethodNotAllowedException("Outcome can not be calculated until heist is FINISHED");
        }

        double memberPercentage = (double) heist.getMembers().size() / HeistUtil.requiredMembers(heist);
        int outcomeLevel = defineOutcomeLevel(memberPercentage);

        if (memberPercentage != 1.0 && shouldMemberStatusesBeSet(heist.getMembers())) {
            setMembersStatuses(heist.getMembers(), STATUSES[outcomeLevel]);
        }
        return OUTCOMES[outcomeLevel];
    }

    private void setMembersStatuses(Set<Member> members, StatusPossibility status) {
        for (Member member : members) {
            setMemberStatus(status, member);
        }
    }

    private int defineOutcomeLevel(double memberPercentage) {
        if (memberPercentage < 0.5) {
            return 0;
        } else if (memberPercentage < 0.75) {
            if (random.nextDouble() > 0.5) {
                return 1;
            } else {
                return 2;
            }
        } else if (memberPercentage < 0.1) {
            return 3;
        } else {
            return 4;
        }
    }

    private void setMemberStatus(StatusPossibility s1, Member m) {
        Status definedStatus = s1.statuses.get(random.nextInt(s1.statuses.size()));
        if (random.nextDouble() <= s1.possibility) memberService.setStatus(m.getId(), definedStatus);
    }

    private static boolean shouldMemberStatusesBeSet(Set<Member> members) {
        return members.stream()
                .noneMatch(m -> m.getStatus().equals(Status.EXPIRED) || m.getStatus().equals(Status.INCARCERATED));
    }

    private static class StatusPossibility {
        double possibility;
        List<Status> statuses;

        public StatusPossibility(double possibility, List<Status> statuses) {
            this.possibility = possibility;
            this.statuses = statuses;
        }
    }

}
