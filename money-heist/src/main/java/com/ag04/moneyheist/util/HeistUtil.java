package com.ag04.moneyheist.util;

import com.ag04.moneyheist.domain.Heist;
import com.ag04.moneyheist.domain.HeistStatus;
import com.ag04.moneyheist.dto.HeistDto;
import com.ag04.moneyheist.dto.SkillDto;
import com.ag04.moneyheist.exceptions.BadRequestException;
import com.ag04.moneyheist.validators.Validator;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Collection of static methods frequently used in services connected with {@code Member} and {@code Heist} classes.
 *
 * @author dlokner
 */
public class HeistUtil {

    public static int requiredMembers(Heist heist) {
        return heist.getSkills().stream().reduce(0, (r, h) -> r + h.getMembers(), Integer::sum);
    }

    public static boolean canBeStarted(HeistDto heist, Instant now) {
        if (!HeistStatus.READY.name().equals(heist.getStatus())) return false;
        return now.compareTo(Instant.parse(heist.getStartTime())) >= 0 && now.compareTo(Instant.parse(heist.getEndTime())) < 0;
    }

    public static boolean canBeFinished(HeistDto heist, Instant now) {
        if (!HeistStatus.IN_PROGRESS.name().equals(heist.getStatus())) return false;
        return now.compareTo(Instant.parse(heist.getEndTime())) > 0;
    }

    public static Collection<String> redundantSkills(List<? extends SkillDto> s1, List<? extends SkillDto> s2) {
        if (Objects.isNull(s1) || Objects.isNull(s2)) return new LinkedList<>();
        Collection<String> skillNames = s1.stream().map(SkillDto::getName).collect(Collectors.toList());
        return s2.stream().map(SkillDto::getName).filter(skillNames::contains).collect(Collectors.toList());
    }

    public static String errorsString(Map<String, String> errors) {
        return errors.entrySet().stream()
                .reduce("", (s, e) -> s + e.getKey() + "-" + e.getValue() + ";", String::concat);
    }

    public static <T> void checkForBadRequest (T object, Validator<T> validator) {
        Map<String, String> errors = validator.validate(object);
        if (!errors.isEmpty()) {
            String errorMessage = errorsString(errors);
            throw new BadRequestException(errorMessage);
        }
    }
}
