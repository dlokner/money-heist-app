package com.ag04.moneyheist.util;

import com.ag04.moneyheist.domain.Heist;
import com.ag04.moneyheist.domain.Status;
import com.ag04.moneyheist.dto.*;
import com.ag04.moneyheist.repositories.HeistRepository;
import com.ag04.moneyheist.services.MemberService;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Search object for {@code Members} eligible in {@code Heist}
 *
 * @author dlokner
 */
@Component
public class EligibleMembersSearch {

    public static List<Status> STATUSES = Arrays.asList(Status.AVAILABLE, Status.RETIRED);

    private final HeistRepository heistRepository;
    private final MemberService memberService;

    public EligibleMembersSearch(HeistRepository heistRepository, MemberService memberService) {
        this.heistRepository = heistRepository;
        this.memberService = memberService;
    }

    public Collection<EligibleMemberDto> searchForMembers(HeistDto heistDto) {
        Collection<MemberDto> allMembers = memberService.getAllMembers();
        return filterEligibleMembers(heistDto, allMembers);
    }

    public Collection<EligibleMemberDto> membersDtoToEligibleMembersDto(HeistDto heistDto, Collection<MemberDto> members) {
        Collection<EligibleMemberDto> eligibleMembers = new LinkedList<>();
        for (MemberDto m : members) {
            EligibleMemberDto em = new EligibleMemberDto();
            em.setName(m.getName());
            em.setSkills(findEligibleSkills(heistDto.getSkills(), m.getSkills()));
            eligibleMembers.add(em);
        }
        return eligibleMembers;
    }

    public Collection<EligibleMemberDto> filterEligibleMembers(HeistDto heistDto, Collection<MemberDto> members) {
        Collection<EligibleMemberDto> eligibleMembers = new LinkedList<>();
        Collection<Heist> heistsInInterval = getHeistsInInterval(heistDto);
        for (MemberDto member : members) {
            if (!checkStatus(member) || checkIfHeistsContainMember(heistsInInterval, member)) {
                continue;
            }
            Collection<MemberSkillDto> eligibleSkills = findEligibleSkills(heistDto.getSkills(), member.getSkills());
            if (!eligibleSkills.isEmpty()) {
                EligibleMemberDto newMember = new EligibleMemberDto(member.getName(), eligibleSkills);
                eligibleMembers.add(newMember);
            }
        }
        return eligibleMembers;
    }

    private static Collection<MemberSkillDto> findEligibleSkills(Collection<? extends HeistSkillDto> heistSkills,
                                                          Collection<? extends MemberSkillDto> memberSkills) {
        Collection<MemberSkillDto> eligibleMemberSkills = new LinkedList<>();
        for (HeistSkillDto heistSkill : heistSkills) {
            for (MemberSkillDto memberSkill : memberSkills) {
                if (heistSkill.getName().equals(memberSkill.getName()) &&
                        heistSkill.getLevel().length() <= memberSkill.getLevel().length()) {
                    eligibleMemberSkills.add(memberSkill);
                }
            }
        }
        return eligibleMemberSkills;
    }

    private static boolean checkStatus(MemberDto memberDto) {
        Status memberStatus = Status.valueOf(memberDto.getStatus());
        return STATUSES.contains(memberStatus);
    }

    private Collection<Heist> getHeistsInInterval(HeistDto heistDto) {
        return heistRepository.findOutOfInterval(Instant.parse(heistDto.getStartTime()), Instant.parse(heistDto.getEndTime()));
    }

    private boolean checkIfHeistsContainMember(Collection<Heist> heists, MemberDto member) {
        for (Heist h : heists) {
            if (h.getMembers().stream().anyMatch(m -> m.getId().equals(member.getId()))) {
                return true;
            }
        }
        return false;
    }

}
